import random
from dataclasses import dataclass

from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup
from telebot.util import escape

from mtg.mtg_tournament_player import MtgTournamentPlayer
from mtg.mtg_player import MtgPlayer

from tg.utils import get_user_tag


@dataclass
class MtgTournament:
    tournament_id: int
    game_id: int
    round_num: int
    total_rounds: int
    message_id: int
    creator_id: int
    tournament_players: list[MtgTournamentPlayer]
    pairings: list[list[MtgTournamentPlayer]]
    bye: MtgTournamentPlayer

    def to_json(self):
        pairings = []
        for pairing in self.pairings:
            pairings.append(MtgTournamentPlayer.to_json_list(pairing))

        obj = dict()
        obj["tournament_id"] = self.tournament_id
        obj["game_id"] = self.game_id
        obj["round_num"] = self.round_num
        obj["total_rounds"] = self.total_rounds
        obj["message_id"] = self.message_id
        obj["creator_id"] = self.creator_id
        obj["tournament_players"] = MtgTournamentPlayer.to_json_list(self.tournament_players)
        obj["pairings"] = pairings
        if self.bye is not None:
            obj["bye"] = self.bye.to_json()
        else:
            obj["bye"] = None
        return obj

    @staticmethod
    def replace_pair_from_list(pair: list[MtgTournamentPlayer], lst: list[MtgTournamentPlayer]):
        for tournament_player in lst:
            if pair[0] == tournament_player:
                pair[0] = tournament_player
            elif pair[1] == tournament_player:
                pair[1] = tournament_player

    @staticmethod
    def find_player_in_list(player: MtgTournamentPlayer, lst: list[MtgTournamentPlayer]):
        for item in lst:
            if player == item:
                return item

    @classmethod
    def from_json(cls, obj):
        tournament_players = MtgTournamentPlayer.from_json_list(obj["tournament_players"])

        pairings = []
        for item in obj["pairings"]:
            pairing = MtgTournamentPlayer.from_json_list(item)
            cls.replace_pair_from_list(pairing, tournament_players)
            pairings.append(pairing)

        if obj["bye"] is not None:
            bye = cls.find_player_in_list(MtgTournamentPlayer.from_json(obj["bye"]), tournament_players)
        else:
            bye = None

        return cls(
            tournament_id=obj["tournament_id"],
            game_id=obj["game_id"],
            round_num=obj["round_num"],
            total_rounds=obj["total_rounds"],
            message_id=obj["message_id"],
            creator_id=obj["creator_id"],
            tournament_players=tournament_players,
            pairings=pairings,
            bye=bye
        )

    def __is_round_over(self):
        for tournament_player in self.tournament_players:
            if not tournament_player.recorded:
                return False
        return True

    @staticmethod
    def __generate_pairings_no_bye(pairings: list[list[MtgTournamentPlayer]], tournament_players: list[MtgTournamentPlayer]) -> bool:
        if not tournament_players:
            return True
        first_tournament_player = tournament_players[0]
        del tournament_players[0]
        for i, tournament_player in enumerate(tournament_players):
            if tournament_player.player not in first_tournament_player.played_against:
                new_pairings = pairings.copy()
                new_pairings.append([first_tournament_player, tournament_player])

                new_players = tournament_players.copy()
                del new_players[i]

                if MtgTournament.__generate_pairings_no_bye(new_pairings, new_players):
                    pairings.clear()
                    pairings.extend(new_pairings)
                    return True
        return False

    def regenerate_pairings(self, force=False):
        if not force:
            if not self.__is_round_over():
                return False

        for tournament_player in self.tournament_players:
            tournament_player.recorded = False
        self.round_num += 1

        tournament_players = self.tournament_players.copy()
        random.shuffle(tournament_players)
        tournament_players.sort(key=lambda x: x.score if not x.received_bye else x.score - 1)

        if len(tournament_players) % 2 != 0:
            for i, tournament_player in enumerate(tournament_players):
                if not tournament_player.received_bye:
                    self.bye = tournament_player
                    self.bye.received_bye = True
                    self.bye.score += 3
                    self.bye.recorded = True
                    del tournament_players[i]
                    break
            random.shuffle(tournament_players)

        tournament_players.sort(key=lambda x: x.score if not x.received_bye else x.score - 1, reverse=True)

        self.pairings = []
        self.__generate_pairings_no_bye(self.pairings, tournament_players)
        for pairing in self.pairings:
            pairing[0].played_against.append(pairing[1].player)
            pairing[1].played_against.append(pairing[0].player)

        return True

    async def create_message(self, chat_id):
        if not self.pairings:
            self.regenerate_pairings(force=True)

        msg = f'[{self.tournament_id}] Турнир по игре с номером {self.game_id}\n'
        msg += f'=[ Раунд {self.round_num} / {self.total_rounds} ]=\n'
        msg += '=== Паринги ===\n'

        for pairing in self.pairings:
            status = '⏳' if not pairing[0].recorded else '✅'
            msg += f'[{status}] {escape(pairing[0].player.display_name)} vs. {escape(pairing[1].player.display_name)}\n'
        if self.bye is not None:
            msg += f'bye: {escape(self.bye.player.display_name)}'

        msg += '\n=== Промежуточные итоги ===\n'
        self.tournament_players.sort(key=lambda x: x.score if not x.received_bye else x.score - 1, reverse=True)
        for tournament_player in self.tournament_players:
            tag = await get_user_tag(chat_id, tournament_player.player.telegram_id)
            if tag == '[ошибка]':
                msg += f'- {escape(tournament_player.player.display_name)}'
            else:
                msg += f'- {escape(tournament_player.player.display_name)} ({tag})'
            msg += f': {tournament_player.score}\n'

        keyboard = [
            [InlineKeyboardButton(text="Я победил", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "add win"}}')],
            [InlineKeyboardButton(text="Была ничья", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "add draw"}}')],
            [InlineKeyboardButton(text="Я проиграл", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "add loss"}}')],
        ]
        if self.round_num != self.total_rounds:
            keyboard.append([InlineKeyboardButton(text="Сгенерировать новые паринги", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "generate pairings"}}')])
        else:
            keyboard.append([InlineKeyboardButton(text="Подвести итоги турнира", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "results"}}')])
        keyboard.append([InlineKeyboardButton(text="Уйти с турнира", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "quit tournament"}}')])
        keyboard.append([InlineKeyboardButton(text="Удалить турнир", callback_data=f'{{ "tournament_id": {self.tournament_id}, "action": "delete tournament"}}')])

        return msg, InlineKeyboardMarkup(keyboard)

    def __find_pair(self, tournament_player: MtgTournamentPlayer):
        for pairing in self.pairings:
            if pairing[0] == tournament_player:
                return pairing[1]
            elif pairing[1] == tournament_player:
                return pairing[0]
        return None

    def add_win(self, player: MtgPlayer):
        for tournament_player in self.tournament_players:
            if tournament_player.player == player:
                if not tournament_player.recorded:
                    pair = self.__find_pair(tournament_player)
                    if pair is None:
                        return 'Внутренняя ошибка'
                    tournament_player.recorded = True
                    tournament_player.score += 3
                    pair.recorded = True
                    return f'Записал победу игрока {player.display_name} против игрока {pair.player.display_name}!', True
                else:
                    return 'Ошибка! В данном кругу данные уже записаны', False
        return f'Ошибка! Кажется, игрок {player.display_name} не участвует в турнире', False

    def add_draw(self, player: MtgPlayer):
        for tournament_player in self.tournament_players:
            if tournament_player.player == player:
                if not tournament_player.recorded:
                    pair = self.__find_pair(tournament_player)
                    if pair is None:
                        return 'Внутренняя ошибка'
                    tournament_player.recorded = True
                    tournament_player.score += 1
                    pair.score += 1
                    pair.recorded = True
                    return f'Записал ничью игроков {player.display_name} и {pair.player.display_name}!', True
                else:
                    return 'Ошибка! В данном кругу данные уже записаны', False
        return f'Ошибка! Кажется, игрок {player.display_name} не участвует в турнире', False

    def add_loss(self, player: MtgPlayer):
        for tournament_player in self.tournament_players:
            if tournament_player.player == player:
                if not tournament_player.recorded:
                    pair = self.__find_pair(tournament_player)
                    if pair is None:
                        return 'Внутренняя ошибка'
                    tournament_player.recorded = True
                    pair.score += 3
                    pair.recorded = True
                    return f'Записал поражение игрока {player.display_name} против игрока {pair.player.display_name}!', True
                else:
                    return 'Ошибка! В данном кругу данные уже записаны', False
        return f'Ошибка! Кажется, игрок {player.display_name} не участвует в турнире', False

    def remove_player(self, player: MtgPlayer):
        for i, tournament_player in enumerate(self.tournament_players):
            if tournament_player.player == player:
                if not tournament_player.recorded:
                    pair = self.__find_pair(tournament_player)
                    if pair is None:
                        return 'Внутренняя ошибка'
                    # Needed to update recorded state in pairings if deleted player is the first one
                    tournament_player.recorded = True
                    pair.score += 3
                    pair.recorded = True
                    del self.tournament_players[i]
                    return f'Удалил игрока {player.display_name} и зачёл победу игроку {pair.player.display_name}!', True
                else:
                    del self.tournament_players[i]
                    return 'Удалил игрока {player.display_name}', True
        return f'Ошибка! Кажется, игрок {player.display_name} не участвует в турнире', False

    async def create_results_msg(self, chat_id):
        if not self.__is_round_over():
            return None

        if self.round_num != self.total_rounds:
            return None

        prev = -1
        last = -1
        results = []
        place_counter = 1
        byes = False
        for tournament_player in sorted(self.tournament_players, key=lambda x: x.score if not x.received_bye else x.score - 1, reverse=True):
            tag = await get_user_tag(chat_id, tournament_player.player.telegram_id)
            if tag == '[ошибка]':
                display_text = f'{escape(tournament_player.player.display_name)}'
            else:
                display_text = f'{escape(tournament_player.player.display_name)} ({tag})'

            if place_counter in {1, 2, 3}:
                place = ['🥇', '🥈', '🥉'][place_counter - 1]
            else:
                place = f'{place_counter}'
            place_counter += 1

            if tournament_player.score == prev and tournament_player.received_bye == byes:
                results[last]["players"].append(display_text)
            else:
                if tournament_player.received_bye:
                    place += f' ({tournament_player.score} pts, bye)'
                else:
                    place += f' ({tournament_player.score} pts)'
                results.append({"place": place, "players": [display_text]})
                last += 1
            prev = tournament_player.score
            byes = tournament_player.received_bye

        msg = f'Результаты турнира № {self.tournament_id}:\n'
        for result in results:
            msg += f'{result["place"]}: '
            for display_text in result["players"]:
                msg += f'{display_text}, '
            msg = msg.removesuffix(', ')
            msg += '\n'
        return msg
