import json

from dataclasses import dataclass
from datetime import timedelta
from datetime import datetime
from datetime import timezone

from telebot.types import InputFile

from mtg.mtg_player import MtgPlayer
from mtg.mtg_game_manager import game_manager

from tg.utils import get_user_tag
from tg.vars import tg_bot


@dataclass
class MtgPoll:
    creator_id: int
    message_id: int
    pref_message_id: int
    poll_id: str
    pref_poll_id: str
    results: dict
    preferences: dict
    creation_date: datetime
    is_active: bool

    chat_id: int
    scheduled_start_day: int
    scheduled_start_hour: int
    scheduled_stop_day: int
    scheduled_stop_hour: int

    def to_json(self):
        obj = dict()
        obj["creator_id"] = self.creator_id
        obj["message_id"] = self.message_id
        obj["pref_message_id"] = self.pref_message_id
        obj["poll_id"] = self.poll_id
        obj["pref_poll_id"] = self.pref_poll_id
        obj["results"] = self.results
        obj["preferences"] = self.preferences
        try:
            obj["creation_date"] = self.creation_date.timestamp()
        except:
            obj["creation_date"] = 0
        obj["is_active"] = self.is_active
        obj["chat_id"] = self.chat_id
        obj["scheduled_start_day"] = self.scheduled_start_day
        obj["scheduled_start_hour"] = self.scheduled_start_hour
        obj["scheduled_stop_day"] = self.scheduled_stop_day
        obj["scheduled_stop_hour"] = self.scheduled_stop_hour
        return obj

    def save_to_file(self, path='./data/poll.json'):
        with open(path, mode='w', encoding='utf-8') as f:
            json.dump(self.to_json(), f, ensure_ascii=False, indent=4)

    @classmethod
    def from_json(cls, obj):
        return cls(
            creator_id=obj["creator_id"],
            message_id=obj["message_id"],
            pref_message_id=obj["pref_message_id"],
            poll_id=obj["poll_id"],
            pref_poll_id=obj["pref_poll_id"],
            results=obj["results"],
            preferences=obj["preferences"],
            creation_date=datetime.fromtimestamp(obj["creation_date"]),
            is_active=obj["is_active"],
            chat_id=obj["chat_id"],
            scheduled_start_day=obj["scheduled_start_day"],
            scheduled_start_hour=obj["scheduled_start_hour"],
            scheduled_stop_day=obj["scheduled_stop_day"],
            scheduled_stop_hour=obj["scheduled_stop_hour"],
        )

    @classmethod
    def load_from_file(cls, path='./data/poll.json'):
        with open(path, mode='r', encoding='utf-8') as f:
            poll = cls.from_json(json.load(f))
        return poll

    @classmethod
    def clear_poll_file(cls, path='./data/poll.json'):
        poll = cls.load_from_file()
        current_date = datetime.now(tz=timezone(timedelta(hours=3)))
        dummy_poll = cls(0, 0, 0, '', '', dict(), dict(), current_date, False, 0, 0, 0, 0, 0)
        if poll.scheduled_start_day != 0 or poll.scheduled_start_hour != 0 or poll.scheduled_stop_day != 0 or poll.scheduled_stop_hour != 0:
            dummy_poll.scheduled_start_day = poll.scheduled_start_day
            dummy_poll.scheduled_start_hour = poll.scheduled_start_hour
            dummy_poll.scheduled_stop_day = poll.scheduled_stop_day
            dummy_poll.scheduled_stop_hour = poll.scheduled_stop_hour
            dummy_poll.creator_id = poll.creator_id
            dummy_poll.chat_id = poll.chat_id
        dummy_poll.save_to_file(path)

    @classmethod
    async def schedule_poll(cls, day_start, hour_start, day_end, hour_end, chat_id, user_id):
        poll = cls.load_from_file()
        if poll.is_active:
            await tg_bot.send_message(chat_id, 'Ошибка! В данный момент уже есть активный опрос')
            return

        if day_start not in range(1, 8) or day_end not in range(1, 8) or day_end <= day_start:
            await tg_bot.send_message(chat_id, 'Ошибка! Неверно указаны дни начала и окончания опроса')
            return

        if hour_start not in range(10, 23) or hour_end not in range(10, 23):
            await tg_bot.send_message(chat_id, 'Ошибка! Неверно указано время начало. Допустимое время - с 10 до 22')
            return

        poll.scheduled_start_day = day_start
        poll.scheduled_start_hour = hour_start
        poll.scheduled_stop_day = day_end
        poll.scheduled_stop_hour = hour_end
        poll.creator_id = user_id
        poll.chat_id = chat_id
        poll.creation_date = datetime.now()
        poll.save_to_file()

        day_start_str = ['', 'каждый понедельник', 'каждый вторник', 'каждую среду', 'каждый четверг', 'каждую пятницу', 'каждую субботы', 'каждое воскресенье'][day_start]
        day_end_str = ['', 'каждый понедельник', 'каждый вторник', 'каждую среду', 'каждый четверг', 'каждую пятницу', 'каждую субботы', 'каждое воскресенье'][day_end]

        await tg_bot.send_message(chat_id, f'Готово! Буду присылать опрос {day_start_str} в {hour_start}:00 и закрывать {day_end_str} в {hour_end}:00. Организатором созданных игр будет {await get_user_tag(chat_id, user_id)}')

    @classmethod
    def is_poll_scheduled_start(cls):
        current_date = datetime.now(tz=timezone(timedelta(hours=3)))

        if not current_date.hour in range(10, 23):
            return False

        poll = cls.load_from_file()
        if poll.scheduled_start_day == 0 or poll.scheduled_start_hour == 0 or poll.scheduled_stop_day == 0 or poll.scheduled_stop_hour == 0:
            return False
        if not poll.is_active and (current_date.weekday() + 1 >= poll.scheduled_start_day) and (current_date.hour >= poll.scheduled_start_hour) and (current_date.weekday() + 1 < poll.scheduled_stop_day):
            return True
        return False

    @classmethod
    def is_poll_scheduled_close(cls):
        current_date = datetime.now(tz=timezone(timedelta(hours=3)))

        if not current_date.hour in range(10, 23):
            return False

        poll = cls.load_from_file()
        if poll.scheduled_start_day == 0 or poll.scheduled_start_hour == 0 or poll.scheduled_stop_day == 0 or poll.scheduled_stop_hour == 0:
            return False
        if poll.is_active and (current_date.weekday() + 1 >= poll.scheduled_stop_day) and (current_date.hour >= poll.scheduled_stop_hour):
            return True
        return False

    @classmethod
    async def send_poll_for_next_week(cls, chat_id=0, user_id=0):
        poll = cls.load_from_file()
        current_date = datetime.now(tz=timezone(timedelta(hours=3)))

        if chat_id == 0:
            if poll.chat_id == 0:
                return
            else:
                chat_id = poll.chat_id
        if user_id == 0:
            if poll.creator_id == 0:
                return
            else:
                user_id = poll.creator_id

        if poll.is_active:
            await tg_bot.send_message(chat_id, 'Ошибка! В данный момент уже есть активный опрос')
            return

        next_week = current_date + timedelta(days=7)
        while next_week.weekday() != 0:
            next_week -= timedelta(days=1)

        res = await tg_bot.send_poll(chat_id, 'Голосуем за коммандер на следующей неделе:',
           [
               f'Понедельник ({(next_week + timedelta(days=0)).day:02}.{(next_week + timedelta(days=0)).month:02})',
               f'Вторник ({(next_week + timedelta(days=1)).day:02}.{(next_week + timedelta(days=1)).month:02})',
               f'Среда ({(next_week + timedelta(days=2)).day:02}.{(next_week + timedelta(days=2)).month:02})',
               f'Четверг ({(next_week + timedelta(days=3)).day:02}.{(next_week + timedelta(days=3)).month:02})',
               f'Пятница ({(next_week + timedelta(days=4)).day:02}.{(next_week + timedelta(days=4)).month:02})',
               'Проверяю обстановку 👀',
               'Не приду'
           ], is_anonymous=False, allows_multiple_answers=True
        )
        try:
            await tg_bot.pin_chat_message(chat_id, res.id)
        except:
            pass

        poll.message_id = res.id
        poll.poll_id = res.poll.id

        res = await tg_bot.send_poll(chat_id, 'Сколько готов играть на неделе?',
            [
                'Не больше 1 раза в неделю',
                'Не больше 2 раз в неделю',
                'Не больше 3 раз в неделю',
                'Не больше 4 раз в неделю',
                'Хоть каждый день'
            ], is_anonymous=False, allows_multiple_answers=False
        )
        poll.pref_message_id = res.id
        poll.pref_poll_id = res.poll.id

        poll.creator_id = user_id
        poll.results = dict()
        poll.creation_date = current_date
        poll.is_active = True
        poll.save_to_file()

    def add_result(self, user_id, votes):
        self.results[f"{user_id}"] = votes

    def add_prefs(self, user_id, max_game_count):
        self.preferences[f"{user_id}"] = max_game_count

    @classmethod
    async def process_poll_results(cls, chat_id=0, user_id=0):
        poll = cls.load_from_file()

        if chat_id == 0:
            if poll.chat_id == 0:
                return
            else:
                chat_id = poll.chat_id
        if user_id == 0:
            if poll.creator_id == 0:
                return
            else:
                user_id = poll.creator_id

        if not poll.is_active:
            await tg_bot.send_message(chat_id, 'Ошибка! В данный момент нет активного опроса')
            return

        if poll.creator_id != user_id:
            await tg_bot.send_message(chat_id, 'Ошибка! Только тот, кто создал опрос может его закрыть')
            return

        cls.clear_poll_file()
        try:
            await tg_bot.stop_poll(chat_id, poll.message_id)
            await tg_bot.stop_poll(chat_id, poll.pref_message_id)
            await tg_bot.unpin_chat_message(chat_id, poll.message_id)
        except:
            pass

        #        Пн Вт Ср Чт Пт Пр Не
        votes = [0, 0, 0, 0, 0, 0, 0]
        players: list[list[MtgPlayer]] = [[], [], [], [], [], [], []]

        for telegram_id in poll.results:
            try:
                chat_member = await tg_bot.get_chat_member(chat_id, int(telegram_id))
            except:
                continue
            if chat_member.status == 'left':
                continue
            for vote in poll.results[telegram_id]:
                votes[vote] += 1
                players[vote].append(MtgPlayer(chat_member.user.full_name, False, chat_member.user.id))

        day_votes = votes[:5]
        day_players = players[:5]
        max_votes = max(day_votes)

        if max_votes <= 2:
            await tg_bot.send_message(chat_id, 'Опрос завершён, но проголосовало слишком мало игроков. Нет ни одного дня с 3+ голосами. Пожалуйста, создайте игры вручную, если всё равно хотите сыграть')
            return

        poll_next_week = poll.creation_date + timedelta(days=7)
        while poll_next_week.weekday() != 0:
            poll_next_week -= timedelta(days=1)
        days = (
            f'{(poll_next_week + timedelta(days=0)).day:02}.{(poll_next_week + timedelta(days=0)).month:02} (Пн)',
            f'{(poll_next_week + timedelta(days=1)).day:02}.{(poll_next_week + timedelta(days=1)).month:02} (Вт)',
            f'{(poll_next_week + timedelta(days=2)).day:02}.{(poll_next_week + timedelta(days=2)).month:02} (Ср)',
            f'{(poll_next_week + timedelta(days=3)).day:02}.{(poll_next_week + timedelta(days=3)).month:02} (Чт)',
            f'{(poll_next_week + timedelta(days=4)).day:02}.{(poll_next_week + timedelta(days=4)).month:02} (Пт)',
        )

        with open('./data/default_place.txt', mode='r', encoding='utf-8') as f:
            default_place = f.read()

        added_players_dict = {}
        added_players = []
        wanting_players = players[5]
        dates = ''

        for i, res in sorted(enumerate(day_votes), key=lambda x: max_votes - x[1]):
            if res >= 3:
                voted_players = []
                for voted_player in day_players[i]:
                    if f'{voted_player.telegram_id}' in added_players_dict:
                        if added_players_dict[f"{voted_player.telegram_id}"]["games_left"] > 0:
                            added_players_dict[f"{voted_player.telegram_id}"]["games_left"] -= 1
                            voted_players.append(voted_player)
                    else:
                        games_left = 1
                        if f'{voted_player.telegram_id}' in poll.preferences:
                            games_left = poll.preferences[f"{voted_player.telegram_id}"]
                        added_players_dict[f"{voted_player.telegram_id}"] = {
                            "games_left": games_left - 1,
                            "player": voted_player
                        }
                        voted_players.append(voted_player)
                if len(voted_players) < 3:
                    wanting_players.extend(voted_players)
                    for voted_player in voted_players:
                        added_players_dict[f"{voted_player.telegram_id}"]["games_left"] += 1
                    continue

                game_id = game_manager.new_game('edh')
                game = game_manager.load_game(game_id)
                dates += days[i] + ', '

                game.place = default_place
                game.date = days[i]
                game.players = voted_players
                game.creator_id = poll.creator_id

                msg, markup = await game.create_message(chat_id)
                sent_message = await tg_bot.send_message(chat_id, msg, reply_markup=markup, parse_mode='HTML')
                try:
                    await tg_bot.pin_chat_message(chat_id, sent_message.id)
                except:
                    pass
                game.message_id = sent_message.message_id
                game_manager.save_game(game)

                added_players.extend(voted_players)
            else:
                wanting_players.extend(day_players[i])

        dates = dates.removesuffix(', ')

        with open('./data/register_link.txt', mode='r') as f:
            reg_link = f.read()

        msg = f'''
{dates}, 18:45
{default_place}

MTG: Commander

Приветствуем всех, даже не знакомых с правилами MTG.
Есть несколько свободных колод, всё выдадим, всё расскажем.
Для желающих ознакомиться на официальном <a href="https://magic.wizards.com/ru/formats/commander">сайте</a> есть немного текста с основными правилами. 

Можно записаться <a href="{reg_link}">здесь</a> или связаться с {await get_user_tag(chat_id, user_id)}, он вас запишет.
        '''
        await tg_bot.send_photo(chat_id, InputFile('./data/game_header.jpg'), caption=msg, parse_mode='HTML')

        wanting_players = set(wanting_players)
        added_players = set(added_players)
        wanting_players = wanting_players.difference(added_players)

        msg = f'Опрос завершён, победили следующие даты: {dates}! '
        if wanting_players:
            for player in wanting_players:
                msg += await get_user_tag(chat_id, player.telegram_id)
                msg += ', '
            msg += 'ваши дни не победили (или вы проголосовали только за "Проверяю обстановку"), но вы можете добавиться в созданные игры.'

        await tg_bot.send_message(chat_id, msg, parse_mode='HTML')

        return True
