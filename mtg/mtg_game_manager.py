from mtg.mtg_game import MtgGame

import os
import json
import threading


class GameManager:
    def __init__(self, workdir: str):
        self._workdir = workdir
        self.game_mgr_lock = threading.Lock()

    def load_game(self, game_id: int):
        with self.game_mgr_lock:
            if not os.path.exists(f'{self._workdir}/{game_id}.json'):
                return None
            with open(f'{self._workdir}/{game_id}.json', 'r', encoding='utf-8') as f:
                return MtgGame.from_json(json.load(f))

    def new_game(self, game_type: str):
        with self.game_mgr_lock:
            game_id = self.__get_new_id()
        with open('./data/default_place.txt', mode='r', encoding='utf-8') as f:
            def_place = f.read()
        self.save_game(MtgGame(game_id, 0, 0, "[TBD]", def_place, "", 0, [], "0", True, game_type))
        return game_id

    def save_game(self, game: MtgGame):
        with self.game_mgr_lock:
            with open(f'{self._workdir}/{game.game_id}.json', 'w', encoding='utf-8') as f:
                json.dump(game.to_json(), f, indent=4, ensure_ascii=False)

    def delete_game(self, game_id):
        with self.game_mgr_lock:
            if not os.path.exists(f'{self._workdir}/{game_id}.json'):
                return False
            else:
                os.remove(f'{self._workdir}/{game_id}.json')
                return True

    def __get_new_id(self):
        new_id = 1
        if os.path.exists(f'{self._workdir}/latest_id.txt'):
            with open(f'{self._workdir}/latest_id.txt', 'r') as f:
                new_id = int(f.read())
        with open(f'{self._workdir}/latest_id.txt', 'w') as f:
            f.write(f'{new_id + 1}')
        return new_id


game_manager = GameManager("./data/games")
