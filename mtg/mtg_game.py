import random

from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup
from telebot.util import escape

from mtg.mtg_player import MtgPlayer

from tg.utils import get_user_tag
from tg.utils import get_user_name_with_badges

from dataclasses import dataclass


@dataclass
class MtgGame:
    game_id: int
    message_id: int
    creator_id: int
    date: str
    place: str
    desc: str
    required_player_count: int
    players: list[MtgPlayer]
    price: str
    allow_not_buy: bool
    game_type: str

    @staticmethod
    async def __pretty_print_table(table_number, table_players: list[MtgPlayer], chat_id) -> str:
        res = f'=== Стол №{table_number} ===\n'
        for player in table_players:
            tag = await get_user_tag(chat_id, player.telegram_id)
            if tag == 'ошибка':
                res += f'- {get_user_name_with_badges(player)}\n'
            else:
                res += f'- {get_user_name_with_badges(player)} ({tag})\n'
        res += '\n'
        return res

    def to_json(self):
        obj = dict()
        obj["game_id"] = self.game_id
        obj["message_id"] = self.message_id
        obj["creator_id"] = self.creator_id
        obj["date"] = self.date
        obj["place"] = self.place
        obj["desc"] = self.desc
        obj["req_player_count"] = self.required_player_count
        obj["players"] = MtgPlayer.to_json_list(self.players)
        obj["price"] = self.price
        obj["allow_not_buy"] = self.allow_not_buy
        obj["game_type"] = self.game_type
        return obj

    @classmethod
    def from_json(cls, obj: dict):
        if "allow_not_buy" in obj:
            allow_not_buy = obj["allow_not_buy"]
        else:
            allow_not_buy = True

        return cls(
            game_id=obj["game_id"],
            message_id=obj["message_id"],
            creator_id=obj["creator_id"],
            date=obj["date"],
            place=obj["place"],
            desc=obj["desc"],
            required_player_count=obj["req_player_count"],
            players=MtgPlayer.from_json_list(obj["players"]),
            price=obj["price"],
            allow_not_buy=allow_not_buy,
            game_type=obj["game_type"]
        )

    def is_ready_for_tournament(self):
        if len(self.players) <= 3:
            return False
        elif self.required_player_count == 0:
            return True
        elif len(self.players) == self.required_player_count:
            return True
        else:
            return False

    def add_player(self, player: MtgPlayer) -> (str, bool):
        if self.required_player_count != 0 and len(self.players) == self.required_player_count:
            return 'Упс, кажется, в эту игру уже набралось нужное количество игроков', False

        if player.telegram_id != 0:
            for existing_player in self.players:
                if existing_player.telegram_id == player.telegram_id:
                    return f'Игрок {existing_player.display_name} уже записан!', False
        self.players.append(player)
        return f'Игрок {player.display_name} добавлен в игру № {self.game_id}!', True

    def remove_player(self, player: MtgPlayer) -> (str, bool):
        for existing_player in self.players:
            if (existing_player.telegram_id == 0 and player.telegram_id == 0 and existing_player.display_name == player.display_name) or (player.telegram_id != 0 and existing_player.telegram_id == player.telegram_id):
                self.players.remove(existing_player)
                return f'Игрок {existing_player.display_name} удалён!', True
        return f'Игрок {player.display_name} не записан на игру № {self.game_id}!', False

    def remove_by_name(self, name: str) -> (str, bool):
        for existing_player in self.players:
            if existing_player.display_name == name:
                self.players.remove(existing_player)
                return f'Игрок {existing_player.display_name} удалён!', True
        return f'Игрок {name} не записан на игру № {self.game_id}!', False

    def add_players(self, new_players: list[str]) -> (str, bool):
        if self.required_player_count != 0 and (self.required_player_count - len(self.players) < len(new_players)):
            return 'Упс, кажется, в этой игре нет места под всех игроков', False

        mtg_players = list[MtgPlayer]()
        for player in new_players:
            mtg_players.append(MtgPlayer(player, False, 0))
        self.players += mtg_players
        return f'Добавил игроков {new_players} в игру № {self.game_id}!', True

    def remove_players(self, removed_players: list[str]) -> (str, bool):
        modified = False
        removed_list = list[str]
        for removed_player in removed_players:
            mtg_player = MtgPlayer(removed_player, False, 0)
            if mtg_player in self.players:
                self.players.remove(mtg_player)
                removed_list += removed_player
                modified = True
        return f'Удалил игроков {removed_list} из игры № {self.game_id}!', modified

    def set_take_cards(self, telegram_id, is_buying: bool) -> (str, bool):
        for player in self.players:
            if player.telegram_id == telegram_id:
                if player.is_buying != is_buying:
                    player.is_buying = is_buying
                    return "Записал!", True
                else:
                    return "Эта опция и так выбрана", False
        return "Упс, кажется, тебя нет в списке!", False

    async def create_message(self, chat_id) -> (str, InlineKeyboardMarkup):
        if self.required_player_count != 0:
            count_msg = f' ({len(self.players)}/{self.required_player_count})'
        else:
            count_msg = ''

        if self.game_type == 'edh':
            game_type_str = ' (коммандер)'
        elif self.game_type == 'draft':
            game_type_str = ' (драфт)'
        elif self.game_type == 'std':
            game_type_str = ' (стандарт)'
        else:
            game_type_str = ''

        msg = f'[{self.game_id}] Игра{game_type_str} от {self.date} в {self.place}\n{self.desc}\n=== Список записавшихся{count_msg} ===\n'
        for player in self.players:
            tag = await get_user_tag(chat_id, player.telegram_id)
            if tag == '[ошибка]':
                msg += f'- {get_user_name_with_badges(player)}'
            else:
                msg += f'- {get_user_name_with_badges(player)} ({tag})'
            if self.game_type == 'draft':
                if player.is_buying or not self.allow_not_buy:
                    msg += ' (забирает карты)'
                else:
                    msg += ' (не забирает карты)'
            msg += '\n'

        if self.game_type == 'edh':
            keyboard = [
                [InlineKeyboardButton(text="Записаться на игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "register"}}')],
                [InlineKeyboardButton(text="Отменить запись на игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "unregister"}}')],
                [InlineKeyboardButton(text="Сгенерировать рассадку", callback_data=f'{{ "game_id": {self.game_id}, "action": "generate tables"}}')],
                [InlineKeyboardButton(text="Создать турнир", callback_data=f'{{ "game_id": {self.game_id}, "action": "create tournament"}}')],
                [InlineKeyboardButton(text="Удалить игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "delete game"}}')],
            ]
        elif self.game_type == 'draft':
            participate_price = '' if self.allow_not_buy else f' ({self.price})'

            keyboard = []
            if self.required_player_count == 0 or (self.required_player_count != 0 and len(self.players) < self.required_player_count):
                keyboard.append([InlineKeyboardButton(text=f"Записаться на игру{participate_price}", callback_data=f'{{ "game_id": {self.game_id}, "action": "register"}}')])

            keyboard.append([InlineKeyboardButton(text="Отменить запись на игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "unregister"}}')])
            if self.allow_not_buy:
                keyboard.append([InlineKeyboardButton(text=f"Хочу забрать карты ({self.price})", callback_data=f'{{ "game_id": {self.game_id}, "action": "take cards"}}')])
                keyboard.append([InlineKeyboardButton(text="Не хочу забирать карты (бесплатно)", callback_data=f'{{ "game_id": {self.game_id}, "action": "dont take cards"}}')])
            keyboard.append([InlineKeyboardButton(text="Создать турнир", callback_data=f'{{ "game_id": {self.game_id}, "action": "create tournament"}}')])
            keyboard.append([InlineKeyboardButton(text="Удалить игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "delete game"}}')])
        elif self.game_type == 'std':
            keyboard = [
                [InlineKeyboardButton(text="Записаться на игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "register"}}')],
                [InlineKeyboardButton(text="Отменить запись на игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "unregister"}}')],
                [InlineKeyboardButton(text="Сгенерировать пары", callback_data=f'{{ "game_id": {self.game_id}, "action": "generate tables"}}')],
                [InlineKeyboardButton(text="Создать турнир", callback_data=f'{{ "game_id": {self.game_id}, "action": "create tournament"}}')],
                [InlineKeyboardButton(text="Удалить игру", callback_data=f'{{ "game_id": {self.game_id}, "action": "delete game"}}')],
            ]
        else:
            keyboard = [
                [InlineKeyboardButton(text="Ошибка")]
            ]

        return msg, InlineKeyboardMarkup(keyboard)

    async def generate_tables(self, chat_id, user_id) -> (str, str):
        if self.creator_id != user_id:
            return 'Ошибка! Только создатель игры может генерировать рассадку!', None
        if self.game_type == 'edh':
            return await self.__generate_tables_groups(chat_id)
        elif self.game_type == 'draft' or self.game_type == 'std':
            return await self.__generate_tables_2v2(chat_id)
        else:
            return 'Ошибка! Похоже, что-то не так с этой игрой', None

    async def __generate_tables_2v2(self, chat_id) -> (str, str):
        if (self.required_player_count != 0 and self.required_player_count != len(self.players)) or len(self.players) == 0:
            return 'Ошибка, пока не набралось нужное число игроков', None

        if len(self.players) % 2 != 0:
            return 'Ошибка, кол-во игроков должно быть кратно 2', None

        generation = f'Игра (драфт) от {self.date} в {self.place}:\nРаспределение столов:\n'

        table_count = 1
        remaining_players = self.players.copy()
        random.shuffle(remaining_players)
        while remaining_players:
            generation += await self.__pretty_print_table(table_count, remaining_players[:2], chat_id)
            remaining_players = remaining_players[2:]
            table_count += 1
            pass

        return 'Готово!', generation

    async def __generate_tables_groups(self, chat_id) -> (str, str):
        if len(self.players) < 3:
            return 'Ошибка, слишком мало игроков для игры', None

        total_count = len(self.players)

        generation = f'Игра (коммандер) от {self.date} в {self.place}:\nРаспределение столов:\n'
        if total_count in {3, 4, 5}:
            generation += await self.__pretty_print_table(1, self.players, chat_id)
            return 'Готово!', generation

        tables_5 = 0
        tables_4 = 0
        tables_3 = 0

        if total_count % 4 == 0:
            tables_4 = total_count // 4
        elif total_count % 3 == 0:
            tables_3 = total_count // 3
        elif total_count % 4 in {1, 3}:
            tables_4 = total_count // 4
            if total_count % 4 == 1:
                tables_4 -= 1
                tables_5 = 1
            elif total_count % 4 == 3:
                tables_3 = 1
        elif total_count % 3 in {1}:
            tables_3 = total_count // 3 - 1
            tables_4 = 1
        elif total_count % 5 in {0, 4}:
            tables_5 = total_count // 5
            if total_count % 5 == 4:
                tables_4 = 1
        else:
            tables_3 = total_count // 3 - 1
            tables_5 = 1

        players = self.players.copy()
        table_counter = 1
        random.shuffle(players)
        while tables_5 >= 1:
            generation += await self.__pretty_print_table(table_counter, players[:5], chat_id)
            players = players[5:]
            table_counter += 1
            tables_5 -= 1

        while tables_4 >= 1:
            generation += await self.__pretty_print_table(table_counter, players[:4], chat_id)
            players = players[4:]
            table_counter += 1
            tables_4 -= 1

        while tables_3 >= 1:
            generation += await self.__pretty_print_table(table_counter, players[:3], chat_id)
            players = players[3:]
            table_counter += 1
            tables_3 -= 1

        return 'Готово!', generation
