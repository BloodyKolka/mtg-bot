from dataclasses import dataclass


@dataclass
class MtgPlayer:
    display_name: str
    is_buying: bool
    telegram_id: int

    def __eq__(self, other):
        if self.telegram_id != 0 and self.telegram_id == other.telegram_id:
            return True
        elif self.telegram_id == 0 and self.display_name == other.display_name:
            return True
        else:
            return False

    def __hash__(self):
        if self.telegram_id != 0:
            return hash(self.telegram_id)
        else:
            return hash(self.display_name)

    def to_json(self):
        obj = dict()
        obj["display_name"] = self.display_name
        obj["is_buying"] = self.is_buying
        obj["telegram_id"] = self.telegram_id
        return obj

    @classmethod
    def to_json_list(cls, lst: list):
        json_list = []
        for item in lst:
            json_list.append(item.to_json())
        return json_list

    @classmethod
    def from_json(cls, obj: dict):
        return cls(
            display_name=obj["display_name"],
            is_buying=obj["is_buying"],
            telegram_id=obj["telegram_id"]
        )

    @classmethod
    def from_json_list(cls, json_list: list):
        lst = []
        for item in json_list:
            lst.append(cls.from_json(item))
        return lst
