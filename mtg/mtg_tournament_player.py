from dataclasses import dataclass

from mtg.mtg_player import MtgPlayer


@dataclass
class MtgTournamentPlayer:
    player: MtgPlayer
    score: int
    played_against: list[MtgPlayer]
    received_bye: bool
    recorded: bool

    def __eq__(self, other):
        return self.player == other.player

    def to_json(self):
        obj = dict()
        obj["player"] = self.player.to_json()
        obj["score"] = self.score
        obj["played_against"] = MtgPlayer.to_json_list(self.played_against)
        obj["received_bye"] = self.received_bye
        obj["recorded"] = self.recorded
        return obj

    @classmethod
    def to_json_list(cls, lst):
        json_list = []
        for item in lst:
            json_list.append(item.to_json())
        return json_list

    @classmethod
    def from_json(cls, obj):
        return cls(
            player=MtgPlayer.from_json(obj["player"]),
            score=obj["score"],
            played_against=MtgPlayer.from_json_list(obj["played_against"]),
            received_bye=obj["received_bye"],
            recorded=obj["recorded"]
        )

    @classmethod
    def from_json_list(cls, json_list):
        lst = []
        for item in json_list:
            lst.append(cls.from_json(item))
        return lst

    @classmethod
    def create_list(cls, players: list[MtgPlayer]):
        tournament_players = []
        for player in players:
            tournament_players.append(cls(player, 0, [], False, False))
        return tournament_players
