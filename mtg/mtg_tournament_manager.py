import os
import json
import threading

from mtg.mtg_tournament_player import MtgTournamentPlayer
from mtg.mtg_tournament import MtgTournament
from mtg.mtg_game import MtgGame


class TournamentManager:
    def __init__(self, workdir: str):
        self._workdir = workdir
        self.tournament_mgr_lock = threading.Lock()

    def load_tournament(self, tournament_id: int) -> MtgTournament | None:
        with self.tournament_mgr_lock:
            if not os.path.exists(f'{self._workdir}/{tournament_id}.json'):
                return None
            with open(f'{self._workdir}/{tournament_id}.json', 'r', encoding='utf-8') as f:
                return MtgTournament.from_json(json.load(f))

    def new_tournament(self, game: MtgGame):
        with self.tournament_mgr_lock:
            tournament_id = self.__get_new_id()

        player_count = len(game.players)
        total_rounds = 3
        cur_limit = 8
        while player_count > cur_limit:
            total_rounds += 1
            cur_limit *= 2

        self.save_tournament(MtgTournament(tournament_id, game.game_id, 0, total_rounds, 0, game.creator_id, MtgTournamentPlayer.create_list(game.players), [], None))
        return tournament_id

    def save_tournament(self, tournament: MtgTournament):
        with self.tournament_mgr_lock:
            with open(f'{self._workdir}/{tournament.tournament_id}.json', 'w', encoding='utf-8') as f:
                json.dump(tournament.to_json(), f, indent=4, ensure_ascii=False)

    def delete_tournament(self, tournament_id):
        with self.tournament_mgr_lock:
            if not os.path.exists(f'{self._workdir}/{tournament_id}.json'):
                return False
            else:
                os.remove(f'{self._workdir}/{tournament_id}.json')
                return True

    def __get_new_id(self):
        new_id = 1
        if os.path.exists(f'{self._workdir}/latest_id.txt'):
            with open(f'{self._workdir}/latest_id.txt', 'r') as f:
                new_id = int(f.read())
        with open(f'{self._workdir}/latest_id.txt', 'w') as f:
            f.write(f'{new_id + 1}')
        return new_id


tournament_manager = TournamentManager('./data/tournaments')
