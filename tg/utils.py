import json
import threading

from telebot.util import escape

from mtg.mtg_player import MtgPlayer

from tg.vars import tg_bot


badges_lock = threading.Lock()


async def get_user_tag(chat_id, user_id) -> str:
    try:
        chat_member = await tg_bot.get_chat_member(chat_id, user_id)
        if chat_member.status != 'left':
            if chat_member.user.username:
                return f'@{chat_member.user.username}'
            else:
                return f'<a href="tg://user?id={chat_member.user.id}">{chat_member.user.full_name}</a>'
    except:
        pass
    return '[ошибка]'


def get_user_name_with_badges(player: MtgPlayer):
    with badges_lock:
        with open('./data/badges.json', encoding='utf-8', mode='r') as f:
            badges = json.load(f)
    if f"{player.telegram_id}" in badges:
        return f'{badges[f"{player.telegram_id}"]} {escape(player.display_name)}'
    else:
        return escape(player.display_name)


def convert_day_str(day_str: str) -> int:
    days_short = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
    days_long = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье']

    if day_str.lower() in days_short:
        return days_short.index(day_str.lower()) + 1
    elif day_str.lower() in days_long:
        return days_long.index(day_str.lower()) + 1
    else:
        raise RuntimeError("Invalid Day!")
