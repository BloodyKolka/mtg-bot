import asyncio
import json
import os
import re

import requests
import telebot.types
from telebot.types import InputFile, InputMediaPhoto

from mtg.mtg_game_manager import game_manager
from mtg.mtg_tournament_manager import tournament_manager
from mtg.mtg_player import MtgPlayer
from mtg.mtg_poll import MtgPoll

from tg.vars import tg_bot
from tg.vars import tg_admins

from tg.utils import convert_day_str


def remove_command(text: str, command: str):
    bot_tag = f'@{tg_bot.user.username}'

    command_less = text.replace(f'/{command} ', '')
    command_less = command_less.replace(f'/{command}', '')
    command_less = command_less.replace(f'/{command}{bot_tag} ', '')
    command_less = command_less.replace(f'/{command}{bot_tag}', '')
    return command_less


@tg_bot.message_handler(commands=['create_poll'])
async def handle_create_poll(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return
    await MtgPoll.send_poll_for_next_week(message.chat.id, message.from_user.id)


@tg_bot.message_handler(commands=['close_poll'])
async def handle_close_poll(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return
    await MtgPoll.process_poll_results(message.chat.id, message.from_user.id)


@tg_bot.poll_answer_handler(func=lambda call: True)
async def handle_poll_answer(poll_answer: telebot.types.PollAnswer):
    poll = MtgPoll.load_from_file()
    if (not poll.is_active) or (poll.poll_id != poll_answer.poll_id and poll.pref_poll_id != poll_answer.poll_id):
        return
    if poll.poll_id == poll_answer.poll_id:
        poll.add_result(poll_answer.user.id, poll_answer.option_ids)
    else:
        if not poll_answer.option_ids:
            poll.add_prefs(poll_answer.user.id, 1)
        else:
            poll.add_prefs(poll_answer.user.id, poll_answer.option_ids[0] + 1)
    poll.save_to_file()


@tg_bot.message_handler(commands=['help', 'h'])
async def handle_help(message: telebot.types.Message):
    help_msg = """
**\\=\\=\\= Создание игры \\=\\=\\=**
Для создания игры используются команды
`/create_edh` \\(`/cedh`\\), `/create_draft` \\(`/cdraft`\\) и `/create_std` \\(`/cstd`\\)
\\* edh \\- создать игру в коммандера \\(без возможности задать ограничение на число игроков\\)
\\* draft \\- создать игру\\-драфт с возможностью необходимого кол\\-во игроков и указания взноса
\\* std \\- создать стандартную игру с колодами по 60 карт \\(без возможности задать ограничение на число игроков\\)

Дополнительно эти команды понимают параметры\\. Они указываются в виде `ключ\\-слово: значение` по 1 на строку
Общие параметры: `где`, `когда`, `описание`
Только для драфта: `взнос`, `кол\\-во`, `об\\. взнос`

Пример команды для создания драфта на 8 человек со взносом 600 рублей:
```
/create_draft
взнос: 600 рублей
кол\\-во: 8
где: антикафе "Вкусный чай"
когда: 15 марта 2016 \\(Вт\\)
описание: Собираемся драфтить Commander Legends
об\\. взнос: нет
```
Параметры опциональны, эту информацию можно указать/отредактировать позже

**\\=\\=\\= Редактирование данных об игре после создания \\=\\=\\=**
`/set_place` или `/p` \\- добавить место к игре\\. Нужно указать номер игры \\(`/p 1 кафе`\\)
`/set_date` `/d` \\- добавить время к игре\\. Нужно указать номер игры \\(`/d 1 завтра`\\)
`/set_desc` \\- добавить описание к игре\\. Нужно указать номер игры \\(`/set_desc 1 описание`\\)
`/set_req_count` \\- добавить необходимое кол\\-во игроков\\. Нужно указать номер игры \\(`/set_req_count 1 8`\\)
`/set_price` \\- установить взнос за игру\\. Нужно указать номер игры \\(`/set_price 1 100 рублей`\\)
Примечание: Если эта информация уже указана у игры \\- то она переписывается новой

**\\=\\=\\= Редактирование списка игроков вручную \\=\\=\\=**
`/add_player` или `/a` \\- добавляет игрока\\. Нужно указать имя и номер игры \\(`/a 1 Вася`\\)
`/remove_player` или `/r` \\- убирает игрока\\. Нужно указать имя и номер игры \\(`/r 1 Вася`\\)\\. Так нельзя убрать игрока записавшегося самостоятельно
`/add_players` или `/as` \\- добавить несколько игроков\\. Нужно указать имена через запятую и номер игры \\(`/as 1 Вася,Петя`\\)
`/remove_players` или `/rs` \\- убрать несколько игроков\\. Нужно указать имена через запятую и номер игры \\(`/rs 1 Вася,Петя`\\)\\. Так нельзя убрать игроков записавшихся самостоятельно
`/remove_player_by_name` `/rn` \\- убрать игрока по имени\\. Нужно указать имя и номер игры \\(`/rn 1 Someguy`\\)\\. Убирает в т\\. ч\\. игроков записавшихся самостоятельно по их имени в Telegram на момент записи \\(см\\. сообщение с объявлением игры\\)
"""
    await tg_bot.send_message(message.chat.id, help_msg, parse_mode='MarkdownV2')


@tg_bot.message_handler(commands=['schedule_poll'])
async def handle_schedule_poll(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args_str = remove_command(message.text, "schedule_poll")

    day_start_str = ''
    hour_start_str = ''
    day_end_str = ''
    hour_end_str = ''

    args = args_str.split('\n')
    for arg in args:
        if arg.startswith('день начала: '):
            day_start_str = arg.replace('день начала: ', '').replace(' ', '')
        elif arg.startswith('час начала: '):
            hour_start_str = arg.replace('час начала: ', '').replace(' ', '')
        elif arg.startswith('день окончания: '):
            day_end_str = arg.replace('день окончания: ', '').replace(' ', '')
        elif arg.startswith('час окончания: '):
            hour_end_str = arg.replace('час окончания: ', '').replace(' ', '')

    if (not day_start_str) or (not hour_start_str) or (not day_end_str) or (not hour_end_str):
        await tg_bot.send_message(message.chat.id, 'Ошибка! Должны быть указаны все параметры: "день начала", "час начала", "день окончания", "час окончания"')
        return

    try:
        day_start = convert_day_str(day_start_str)
        hour_start = int(hour_start_str)
        day_end = convert_day_str(day_end_str)
        hour_end = int(hour_end_str)
    except:
        await tg_bot.send_message(message.chat.id, 'Ошибка! Один из параметров указан неверно. Убедитесь, что часы начала и окончания - это числа, а в качестве дней указан день недели')
        return

    await MtgPoll.schedule_poll(day_start, hour_start, day_end, hour_end, message.chat.id, message.from_user.id)


@tg_bot.message_handler(commands=['create_std', 'cstd'])
async def handle_create_std(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    game_id = game_manager.new_game('std')
    game = game_manager.load_game(game_id)
    game.creator_id = message.from_user.id

    args_str = remove_command(message.text, "create_std")
    args_str = remove_command(args_str, "cstd")

    args = args_str.split('\n')
    for arg in args:
        if arg.startswith('где: '):
            arg = arg.replace('где: ', '')
            game.place = arg
        elif arg.startswith('когда: '):
            arg = arg.replace('когда: ', '')
            game.date = arg
        elif arg.startswith('описание: '):
            arg = arg.replace('описание: ', '')
            game.desc = arg

    await tg_bot.send_message(message.chat.id, f'Создал стандартную игру (60 карт) под номером {game_id}!')
    msg, markup = await game.create_message(message.chat.id)
    sent_message = await tg_bot.send_message(message.chat.id, msg, reply_markup=markup, parse_mode='HTML')
    game.message_id = sent_message.message_id
    game_manager.save_game(game)


@tg_bot.message_handler(commands=['create_edh', 'cedh'])
async def handle_create_edh(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    game_id = game_manager.new_game('edh')
    game = game_manager.load_game(game_id)
    game.creator_id = message.from_user.id

    args_str = remove_command(message.text, "create_edh")
    args_str = remove_command(args_str, "cedh")

    args = args_str.split('\n')
    for arg in args:
        if arg.startswith('где: '):
            arg = arg.replace('где: ', '')
            game.place = arg
        elif arg.startswith('когда: '):
            arg = arg.replace('когда: ', '')
            game.date = arg
        elif arg.startswith('описание: '):
            arg = arg.replace('описание: ', '')
            game.desc = arg

    await tg_bot.send_message(message.chat.id, f'Создал EDH-игру под номером {game_id}!')
    msg, markup = await game.create_message(message.chat.id)
    sent_message = await tg_bot.send_message(message.chat.id, msg, reply_markup=markup, parse_mode='HTML')
    game.message_id = sent_message.message_id
    game_manager.save_game(game)


@tg_bot.message_handler(commands=['create_draft', 'cdraft'])
async def handle_create_draft(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    game_id = game_manager.new_game('draft')
    game = game_manager.load_game(game_id)
    game.creator_id = message.from_user.id

    args_str = remove_command(message.text, "create_draft")
    args_str = remove_command(args_str, "cdraft")

    args = args_str.split('\n')
    for arg in args:
        if arg.startswith('где: '):
            arg = arg.replace('где: ', '')
            game.place = arg
        elif arg.startswith('когда: '):
            arg = arg.replace('когда: ', '')
            game.date = arg
        elif arg.startswith('описание: '):
            arg = arg.replace('описание: ', '')
            game.desc = arg
        elif arg.startswith('взнос: '):
            arg = arg.replace('взнос: ', '')
            game.price = arg
        elif arg.startswith('кол-во: '):
            arg = arg.replace('кол-во: ', '')
            game.required_player_count = int(arg)
        elif arg.startswith('об. взнос: '):
            arg = arg.replace('об. взнос: ', '')
            game.allow_not_buy = False if 'да' in arg else True

    await tg_bot.send_message(message.chat.id, f'Создал драфт-игру под номером {game_id}!')
    msg, markup = await game.create_message(message.chat.id)
    sent_message = await tg_bot.send_message(message.chat.id, msg, reply_markup=markup, parse_mode='HTML')
    game.message_id = sent_message.message_id
    game_manager.save_game(game)


async def check_game(game, chat_id):
    if game is None:
        await tg_bot.send_message(chat_id, "Ошибка, такой игры нет!")
        return False
    return True


async def check_tournament(tournament, chat_id):
    if tournament is None:
        await tg_bot.send_message(chat_id, "Ошибка, такого турнира нет!")
        return False
    return True


@tg_bot.message_handler(commands=['kick'])
async def handle_kick(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    await tg_bot.leave_chat(message.chat.id)


@tg_bot.message_handler(commands=['del'])
async def handle_del(message: telebot.types.Message):
    if message.reply_to_message and (message.reply_to_message.from_user.id == tg_bot.user.id):
        try:
            await tg_bot.delete_message(message.chat.id, message.reply_to_message.message_id)
        except:
            pass


@tg_bot.message_handler(commands=['set_place', 'p'])
async def handle_set_place(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'set_place')
    args = remove_command(args, 'p')
    game_id = int(args.split(' ')[0])
    place = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return
    if message.from_user.id != game.creator_id:
        await tg_bot.reply_to(message, "Ошибка, только создатель игры может менять данные о ней!")

    info_changed = game.place != place
    if info_changed:
        game.place = place
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    if info_changed:
        await tg_bot.reply_to(message, f'Добавил место "{place}" в игру с номером {game_id}!')
    else:
        await tg_bot.reply_to(message, f'Место "{place}" уже указано для игры с номером {game_id}!')


@tg_bot.message_handler(commands=['set_date', 'd'])
async def handle_set_date(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'set_date')
    args = remove_command(args, 'd')
    game_id = int(args.split(' ')[0])
    date = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    if message.from_user.id != game.creator_id:
        await tg_bot.reply_to(message, "Ошибка, только создатель игры может менять данные о ней!")

    info_changed = game.date != date
    if info_changed:
        game.date = date
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    if info_changed:
        await tg_bot.reply_to(message, f'Добавил дату "{date}" в игру с номером {game_id}!')
    else:
        await tg_bot.reply_to(message, f'Дата "{date}" уже указана у игры с номером {game_id}!')


@tg_bot.message_handler(commands=['set_desc'])
async def handle_set_desc(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'set_desc')
    game_id = int(args.split(' ')[0])
    desc = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    if message.from_user.id != game.creator_id:
        await tg_bot.reply_to(message, "Ошибка, только создатель игры может менять данные о ней!")

    info_changed = game.desc != desc
    if info_changed:
        game.desc = desc
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    if info_changed:
        await tg_bot.reply_to(message, f'Добавил описание в игру с номером {game_id}!')
    else:
        await tg_bot.reply_to(message, f'У игры {game_id} уже стоит такое описание!')


@tg_bot.message_handler(commands=['set_price'])
async def handle_set_price(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'set_price')
    game_id = int(args.split(' ')[0])
    price = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    if message.from_user.id != game.creator_id:
        await tg_bot.reply_to(message, "Ошибка, только создатель игры может менять данные о ней!")

    info_changed = game.price != price
    if info_changed:
        game.price = price
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    if info_changed:
        await tg_bot.reply_to(message, f'Добавил цену "{price}" в игру с номером {game_id}!')
    else:
        await tg_bot.reply_to(message, f'У игры {game_id} уже стоит такая цена!')


@tg_bot.message_handler(commands=['set_pay_req'])
async def handle_set_pay_req(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'set_pay_req')
    game_id = int(args.split(' ')[0])
    require_pay_str = args.removeprefix(f'{game_id} ')

    if require_pay_str.startswith('да'):
        require_pay = True
    elif require_pay_str.startswith('нет'):
        require_pay = False
    else:
        await tg_bot.reply_to(message, "Ошибка, допускается указывать только 'да' или 'нет'!")
        return

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    if message.from_user.id != game.creator_id:
        await tg_bot.reply_to(message, "Ошибка, только создатель игры может менять данные о ней!")

    info_changed = (game.allow_not_buy != (not require_pay))
    if info_changed:
        game.allow_not_buy = not require_pay
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    if info_changed:
        await tg_bot.reply_to(message, f'Теперь для игры {game_id} взнос {"обязателен" if require_pay else "не обязателен"}')
    else:
        await tg_bot.reply_to(message, f'У игры {game_id} уже стоит {"обязательный" if require_pay else "необязателеный"} взнос!')


@tg_bot.message_handler(commands=['set_req_count'])
async def handle_set_req_count(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'set_req_count')
    game_id = int(args.split(' ')[0])
    count = int(args.removeprefix(f'{game_id} '))

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    if message.from_user.id != game.creator_id:
        await tg_bot.reply_to(message, "Ошибка, только создатель игры может менять данные о ней!")

    info_changed = game.required_player_count != count
    if info_changed:
        game.required_player_count = count
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    if info_changed:
        await tg_bot.reply_to(message, f'Добавил требуемое кол-во игроков ({count}) в игру с номером {game_id}!')
    else:
        await tg_bot.reply_to(message, f'У игры {game_id} и так установлено данное кол-во игроков ({count})!')


@tg_bot.message_handler(commands=['add_player', 'a'])
async def handle_add_player(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'add_player')
    args = remove_command(args, 'a')
    game_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    res_msg, update_required = game.add_player(MtgPlayer(name, False, 0))
    if update_required:
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['remove_player', 'r'])
async def handle_add_player(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'remove_player')
    args = remove_command(args, 'r')
    game_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    res_msg, update_required = game.remove_player(MtgPlayer(name, False, 0))
    if update_required:
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['add_players', 'as'])
async def handle_add_players(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'add_players')
    args = remove_command(args, 'as')
    game_id = int(args.split(' ')[0])
    names = args.removeprefix(f'{game_id} ').split(',')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    res_msg, update_required = game.add_players(names)
    if update_required:
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['remove_players', 'rs'])
async def handle_add_players(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'remove_players')
    args = remove_command(args, 'rs')
    game_id = int(args.split(' ')[0])
    names = args.removeprefix(f'{game_id} ').split(',')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    res_msg, update_required = game.remove_players(names)
    if update_required:
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['remove_player_by_name', 'rn'])
async def handle_add_player(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'remove_player_by_name')
    args = remove_command(args, 'rn')
    game_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{game_id} ')

    game = game_manager.load_game(game_id)
    if not await check_game(game, message.chat.id):
        return

    res_msg, update_required = game.remove_by_name(name)
    if update_required:
        game_manager.save_game(game)
        msg, markup = await game.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, game.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['add_win'])
async def handle_add_player(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'add_win')
    tournament_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{tournament_id} ')

    tournament = tournament_manager.load_tournament(tournament_id)
    if not await check_tournament(tournament, message.chat.id):
        return

    res_msg, update_required = tournament.add_win(MtgPlayer(name, False, 0))
    if update_required:
        tournament_manager.save_tournament(tournament)
        msg, markup = await tournament.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, tournament.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['add_loss'])
async def handle_add_player(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'add_loss')
    tournament_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{tournament_id} ')

    tournament = tournament_manager.load_tournament(tournament_id)
    if not await check_tournament(tournament, message.chat.id):
        return

    res_msg, update_required = tournament.add_loss(MtgPlayer(name, False, 0))
    if update_required:
        tournament_manager.save_tournament(tournament)
        msg, markup = await tournament.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, tournament.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['add_draw'])
async def handle_add_player(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'add_draw')
    tournament_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{tournament_id} ')

    tournament = tournament_manager.load_tournament(tournament_id)
    if not await check_tournament(tournament, message.chat.id):
        return

    res_msg, update_required = tournament.add_draw(MtgPlayer(name, False, 0))
    if update_required:
        tournament_manager.save_tournament(tournament)
        msg, markup = await tournament.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, tournament.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['remove_t_player'])
async def handle_add_player(message):
    if message.from_user.id == message.chat.id:
        return

    args = remove_command(message.text, 'remove_t_player')
    tournament_id = int(args.split(' ')[0])
    name = args.removeprefix(f'{tournament_id} ')

    tournament = tournament_manager.load_tournament(tournament_id)
    if not await check_tournament(tournament, message.chat.id):
        return

    res_msg, update_required = tournament.remove_player(MtgPlayer(name, False, 0))
    if update_required:
        tournament_manager.save_tournament(tournament)
        msg, markup = await tournament.create_message(message.chat.id)
        await tg_bot.edit_message_text(msg, message.chat.id, tournament.message_id, reply_markup=markup, parse_mode='HTML')

    await tg_bot.reply_to(message, res_msg)


@tg_bot.message_handler(commands=['image_card', 'i'])
async def handle_card(message: telebot.types.Message):
    if message.from_user.id == message.chat.id:
        return

    cardname = remove_command(message.text, 'image_card')
    cardname = remove_command(cardname, 'i')

    if not cardname:
        await tg_bot.reply_to(message, 'Укажите имя карты после команды, например `/image_card гора`')
        return

    res = requests.get('https://api.scryfall.com/cards/named', params={'fuzzy': cardname})
    if res.status_code != 404:
        res_json = res.json()
        image_uri = res_json["image_uris"]["png"]
        res = requests.get(image_uri, allow_redirects=True)
        with open('./tmp/image.png', mode='wb') as f:
            f.write(res.content)
        await tg_bot.send_photo(message.chat.id, InputFile('./tmp/image.png'), caption=res_json["name"] + ' (powered by https://scryfall.com/)', reply_to_message_id=message.message_id)
        os.remove('./tmp/image.png')


@tg_bot.message_handler(content_types=['text'])
async def handle_message(message: telebot.types.Message):
    reg_exp = r'\[\[.*\]\]'
    match = re.search(reg_exp, message.text)
    if match:
        name = match.group()
        name = name.replace('[[', '')
        name = name.replace(']]', '')

        res = requests.get('https://api.scryfall.com/cards/named', params={'fuzzy': name})
        if res.status_code != 404:
            res_json = res.json()
            if 'image_uris' in res_json:
                image_uri = res_json["image_uris"]["png"]
                res = requests.get(image_uri, allow_redirects=True)
                with open('./tmp/image.png', mode='wb') as f:
                    f.write(res.content)
                await tg_bot.send_photo(message.chat.id, InputFile('./tmp/image.png'), caption=res_json["name"] + ' (powered by https://scryfall.com/)', reply_to_message_id=message.message_id)
                os.remove('./tmp/image.png')
            else:
                image_uri = res_json["card_faces"][0]["image_uris"]["png"]
                res = requests.get(image_uri, allow_redirects=True)
                with open('./tmp/image1.png', mode='wb') as f:
                    f.write(res.content)

                image_uri = res_json["card_faces"][1]["image_uris"]["png"]
                res = requests.get(image_uri, allow_redirects=True)
                with open('./tmp/image2.png', mode='wb') as f:
                    f.write(res.content)

                await tg_bot.send_media_group(message.chat.id, [
                        InputMediaPhoto(InputFile('./tmp/image1.png'), caption=res_json["name"] + ' (powered by https://scryfall.com/)'),
                        InputMediaPhoto(InputFile('./tmp/image2.png'))
                    ], reply_to_message_id=message.message_id)
                os.remove('./tmp/image1.png')
                os.remove('./tmp/image2.png')


@tg_bot.callback_query_handler(func=lambda call: True)
async def main_callback(call: telebot.types.CallbackQuery):
    chat_id = call.message.chat.id
    message_id = call.message.message_id
    telegram_id = call.from_user.id
    user_name = call.from_user.full_name
    info = json.loads(call.data)
    action = info["action"]
    extra = None

    if chat_id == telegram_id:
        return

    if "game_id" in info:
        game_id = info["game_id"]
        game = game_manager.load_game(game_id)

        if game is None:
            reply = "Ошибка! Кажется, эта игра уже была удалена"
            try:
                await tg_bot.edit_message_text('[игра удалена]', chat_id, message_id)
            except:
                pass
            await tg_bot.answer_callback_query(callback_query_id=call.id, text=reply, show_alert=True)
            return

        update_required = False
        if action == "register":
            reply, update_required = game.add_player(MtgPlayer(user_name, False, telegram_id))
        elif action == "unregister":
            reply, update_required = game.remove_player(MtgPlayer(user_name, False, telegram_id))
        elif action == "take cards":
            reply, update_required = game.set_take_cards(telegram_id, True)
        elif action == "dont take cards":
            reply, update_required = game.set_take_cards(telegram_id, False)
        elif action == "generate tables":
            reply, extra = await game.generate_tables(chat_id, telegram_id)
        elif action == "create tournament":
            if game.creator_id != telegram_id:
                reply = 'Только создатель игры может создать по ней турнир'
            elif not game.is_ready_for_tournament():
                reply = 'Пока не набралось нужное число игроков'
            else:
                tournament_id = tournament_manager.new_tournament(game)
                tournament = tournament_manager.load_tournament(tournament_id)
                msg, markup = await tournament.create_message(chat_id)
                sent_message = await tg_bot.send_message(chat_id, msg, reply_markup=markup, parse_mode='HTML')
                try:
                    await tg_bot.pin_chat_message(chat_id, sent_message.id, disable_notification=True)
                except:
                    pass
                tournament.message_id = sent_message.id
                tournament_manager.save_tournament(tournament)
                reply = 'Турнир успешно создан'
        elif action == 'delete game':
            if game.creator_id != telegram_id:
                reply = 'Только создатель игры может её удалить'
            else:
                if game_manager.delete_game(game_id):
                    try:
                        await tg_bot.edit_message_text('[игра удалена]', chat_id, message_id)
                    except:
                        pass
                    reply = 'Игра удалена'
                else:
                    reply = 'Внутренняя ошибка!'
        else:
            reply = 'Внутренняя ошибка!'

        if action != 'delete game':
            game_manager.save_game(game)
            if update_required:
                msg, markup = await game.create_message(chat_id)
                await tg_bot.edit_message_text(msg, chat_id, game.message_id, reply_markup=markup, parse_mode='HTML')
    elif "tournament_id" in info:
        tournament_id = info["tournament_id"]
        tournament = tournament_manager.load_tournament(tournament_id)

        if tournament is None:
            reply = "Ошибка! Кажется, этот турнир уже был удален"
            try:
                await tg_bot.edit_message_text('[турнир удален]', chat_id, message_id)
            except:
                pass
            await tg_bot.answer_callback_query(callback_query_id=call.id, text=reply, show_alert=True)
            return

        update_required = False
        new_round = False
        if action == "add win":
            reply, update_required = tournament.add_win(MtgPlayer(user_name, False, telegram_id))
        elif action == "add draw":
            reply, update_required = tournament.add_draw(MtgPlayer(user_name, False, telegram_id))
        elif action == "add loss":
            reply, update_required = tournament.add_loss(MtgPlayer(user_name, False, telegram_id))
        elif action == "generate pairings":
            if tournament.creator_id != telegram_id:
                reply = 'Ошибка, только создатель турнира может генерировать новые паринги'
            elif tournament.regenerate_pairings():
                reply = 'Новые паринги сгенерированы'
                update_required = True
                new_round = True
            else:
                reply = 'Ещё не все игроки внесли результаты игр'
        elif action == "results":
            if tournament.creator_id != telegram_id:
                reply = 'Ошибка, только создатель турнира может подводить итоги'
            else:
                msg = await tournament.create_results_msg(chat_id)
                if msg is not None:
                    reply = 'Результаты собраны!'
                    try:
                        await tg_bot.unpin_chat_message(chat_id, tournament.message_id)
                    except:
                        pass
                    await tg_bot.edit_message_reply_markup(chat_id, tournament.message_id)
                    await tg_bot.send_message(chat_id, msg, parse_mode='HTML', reply_to_message_id=tournament.message_id)
                    tournament_manager.delete_tournament(tournament.tournament_id)
                else:
                    reply = 'Ещё не все игроки внесли результаты игр'
        elif action == "quit tournament":
            reply, update_required = tournament.remove_player(MtgPlayer(user_name, False, telegram_id))
        elif action == "delete tournament":
            if tournament.creator_id != telegram_id:
                reply = 'Только создатель турнира может его удалить'
            else:
                if tournament_manager.delete_tournament(tournament.tournament_id):
                    try:
                        await tg_bot.edit_message_text('[турнир удалён]', chat_id, message_id)
                    except:
                        pass
                    reply = 'Турнир удалён'
                else:
                    reply = 'Внутренняя ошибка!'
        else:
            reply = 'Внутренняя ошибка!'

        if action != 'delete tournament' and action != 'results':
            if update_required:
                msg, markup = await tournament.create_message(chat_id)
                if not new_round:
                    await tg_bot.edit_message_text(msg, chat_id, tournament.message_id, reply_markup=markup, parse_mode='HTML')
                else:
                    await tg_bot.edit_message_reply_markup(chat_id, tournament.message_id)
                    sent_msg = await tg_bot.send_message(chat_id, msg, reply_markup=markup, parse_mode='HTML', reply_to_message_id=tournament.message_id)
                    try:
                        await tg_bot.unpin_chat_message(chat_id, tournament.message_id)
                        await tg_bot.pin_chat_message(chat_id, sent_msg.id, disable_notification=True)
                    except:
                        pass
                    tournament.message_id = sent_msg.id
            tournament_manager.save_tournament(tournament)

    else:
        reply = 'Внутренняя ошибка'

    await tg_bot.answer_callback_query(callback_query_id=call.id, text=reply, show_alert=True)
    if extra is not None:
        await tg_bot.send_message(chat_id, extra, parse_mode='HTML')


async def scheduler():
    while True:
        if MtgPoll.is_poll_scheduled_start():
            await MtgPoll.send_poll_for_next_week()
        elif MtgPoll.is_poll_scheduled_close():
            await MtgPoll.process_poll_results()
        await asyncio.sleep(15)  # Run every 0.5 hours


async def __internal_run_tg_bot():
    await asyncio.gather(tg_bot.infinity_polling(), scheduler())


def run_tg_bot():
    asyncio.run(__internal_run_tg_bot())
